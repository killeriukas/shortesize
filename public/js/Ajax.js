//function AddAjaxCallForm(className, onSuccess, onError) {
//    $('.' + className).submit(function () {
//        var form = $(this);
//
//        var url = form.attr('action');
//        var method = form.attr('method');
//        var formData = form.serialize();
//
//        $.ajax(
//            {
//                type: method,
//                url: url,
//                data: formData,
//                success: onSuccess,
//                error: onError
//            }
//        );
//        return false;
//    });
//}

//function AddAjaxOnClick(className, method, url, data, onSuccess, onError)
//{
//    $('.' + className).click(function (e) {
//        e.preventDefault();
//        console.log($(e.target).attr('id'));
//        AddAjax(method, url, data, onSuccess, onError);
//    });
//}

function AddAjax(method, url, data, onSuccess, onError)
{
    $.ajax({
        type: method,
        url: url,
        data: data,
        success: onSuccess,
        error: onError
    });
}