<?php

namespace lib\data_structure;

use Traversable;

final class Dictionary implements \IteratorAggregate
{

    private $data;

    public function __construct(array $values = array())
    {
        if(!empty($values) && isset($values[0])) {
            throw new \InvalidArgumentException('Dictionary does not accept 0 indexed or empty arrays!');
        }

        $this->data = $values;
    }

    public function Add($key, $value)
    {
        if(!$this->Constains($key)) {
            $this->data[$key] = $value;
        } else {
            throw new \BadMethodCallException('You cannot add the same key [' . $key . '] again!');
        }
    }

    public function Set($key, $value)
    {
        if($this->Constains($key)) {
            $this->data[$key] = $value;
        }
    }

    public function Constains($key)
    {
        return isset($this->data[$key]);
    }

    public function Get($key)
    {
        if($this->Constains($key)) {
            return $this->data[$key];
        }
        return null;
    }

    public function Remove($key)
    {
        if($this->Constains($key)) {
            unset($this->data[$key]);
        }
    }

    public function Clear()
    {
        $this->data = array();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }

    public function ToArray()
    {
        return $this->data;
    }
}