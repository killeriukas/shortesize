<?php

namespace lib\data_structure;

use Traversable;

final class GeneralList implements \IteratorAggregate
{
    private $data;
    private $dirty;
    private $count;

    public function __construct(array $data = array())
    {
        if(!empty($data) && !isset($data[0])) {
            throw new \InvalidArgumentException('General list does not accept dictionary key (string) based array!');
        }

        $this->data = $data;
        $this->dirty = true;
        $this->count = $this->Count();
    }

    public function Add($element)
    {
        $this->dirty = true;
        $this->data[] = $element;
    }

    public function Get($index)
    {
        return $this->data[$index];
    }

    public function IsEmpty()
    {
        return $this->Count() == 0;
    }

    public function GetFirst()
    {
        return $this->data[0];
    }

    public function GetLast()
    {
        $last = $this->Count() - 1;
        return $this->data[$last];
    }

    public function RemoveAt($index)
    {
        $this->dirty = true;
        unset($this->data[$index]);
    }

    public function Count()
    {
        if($this->dirty) {
            $this->dirty = false;
            $this->count = count($this->data);
        }
        return $this->count;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }
}