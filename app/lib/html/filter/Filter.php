<?php

namespace lib\html\filter;

final class Filter
{
    private $fieldName;
    private $filters;

    public function __construct($fieldName, array $filters)
    {
        if (!is_string($fieldName)) {
            throw new \Exception('Field name must be string!');
        }

        $this->fieldName = $fieldName;

        if (empty($filters)) {
            throw new \Exception('Filter cannot be without filters!');
        }

        foreach ($filters as $filter) {
            if (!($filter instanceof FilterBase)) {
                throw new \Exception('One of the filters is not from [FilterBase]');
            }
        }

        $this->filters = $filters;
    }

    public function GetFieldName()
    {
        return $this->fieldName;
    }

    public function GetFilters()
    {
        return $this->filters;
    }

}