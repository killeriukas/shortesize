<?php

namespace lib\html;


final class Message
{

    public static function Information($message)
    {
        self::CommonMessage('alert-info', 'Information', $message);
    }

    public static function Warning($message)
    {
        self::CommonMessage('alert-warning', 'Warning', $message);
    }

    public static function Error($message)
    {
        self::CommonMessage('alert-danger', 'Error', $message);
    }

    private static function CommonMessage($alertLevel, $title, $message)
    {
        echo '<div class="alert ' . $alertLevel . '">';
        echo '<strong>' . $title . '</strong>! ' . $message;
        echo '</div>';
    }

    public static function Success($message)
    {
        self::CommonMessage('alert-success', 'Success', $message);
    }
}