<?php

namespace lib\html\table;

use lib\data_structure\GeneralList;
use lib\html\HtmlAttributes;
use lib\html\Tags;

final class Row
{
    private $columns;
    private $attributes;

    public function __construct(HtmlAttributes $attributes = null)
    {
        $this->columns = new GeneralList();
        $this->attributes = $attributes;
    }

    public function AddColumn(Column $column)
    {
        $this->columns->Add($column);
    }

    public function Render()
    {
        echo Tags::Filter('<tr ' . Tags::ATTRIBUTES . '>', $this->attributes);

        /* @var $value Column */
        foreach($this->columns as $value) {
            $value->Render();
        }
        echo '</tr>';
    }

}
