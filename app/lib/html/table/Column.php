<?php

namespace lib\html\table;

use lib\html\HtmlAttributes;
use lib\html\Tags;

final class Column
{
    private $data;
    private $isHeader;
    private $attributes;

    public function __construct($data, $isHeader = false, HtmlAttributes $attributes = null)
    {
        $this->data = $data;
        $this->isHeader = $isHeader;
        $this->attributes = $attributes;
    }

    public function Render()
    {
        $attr = $this->isHeader ? 'th' : 'td';
        echo Tags::Filter('<' . $attr . ' ' . Tags::ATTRIBUTES . '>', $this->attributes);
        echo $this->data;
        echo '</' . $attr . '>';
    }

}
