<?php

namespace lib\html\table;

use lib\data_structure\GeneralList;
use lib\html\HtmlAttributes;
use lib\html\Tags;

final class Table
{

    private $rows;
    private $attributes;

    public function __construct(HtmlAttributes $attributes = null)
    {
        $this->rows = new GeneralList();
        $this->attributes = $attributes;
    }

    public function AddRow(HtmlAttributes $attributes = null)
    {
        $row = new Row($attributes);
        $this->rows->Add($row);
    }

    public function AddColumn(GeneralList $data, HtmlAttributes $attributes = null)
    {
        $this->AddColumnCell($data, false, $attributes);
    }

    public function AddHeader(GeneralList $data, HtmlAttributes $attributes = null)
    {
        $this->AddColumnCell($data, true, $attributes);
    }

    private function AddColumnCell(GeneralList $data, $isHeader, HtmlAttributes $attributes = null)
    {
        foreach($data as $value) {
            $column = new Column($value, $isHeader, $attributes);
            $this->rows->GetLast()->AddColumn($column);
        }
    }

    public function Render()
    {
        echo Tags::Filter('<table ' . Tags::ATTRIBUTES . '>', $this->attributes);
        /* @var $value Row */
        foreach($this->rows as $value) {
            $value->Render();
        }
        echo '</table>';
    }

}
