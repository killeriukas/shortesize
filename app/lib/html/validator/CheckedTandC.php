<?php

namespace lib\html\validator;

class CheckedTandC extends Checked
{

    protected function GetUncheckMessage($value)
    {
        return 'Terms & Conditions must be accepted';
    }

}