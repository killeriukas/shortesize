<?php

namespace lib\html\validator;

use lib\data_structure\Dictionary;

class Match extends ValidatorBase
{

    private $matchTo;

    public function __construct(array $matchTo)
    {
        $this->matchTo = $matchTo;
    }

    public function IsValid($value, Dictionary $valueList)
    {
        foreach($this->matchTo as $matchKey)
        {
            $matchValue = $valueList->Get($matchKey);
            if($value !== $matchValue)
            {
                $this->AddMessage($this->GetUnmatchMessage($value, $matchValue));
                return false;
            }
        }

        return true;
    }

    protected function GetUnmatchMessage($value, $equalizer)
    {
        return 'Current value [' . $value . '] is not equal [' . $equalizer . ']';
    }

}