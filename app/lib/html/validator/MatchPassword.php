<?php

namespace lib\html\validator;

class MatchPassword extends Match
{
    public function __construct(array $matchTo)
    {
        parent::__construct($matchTo);
    }

    protected function GetUnmatchMessage($value, $equalizer)
    {
        return 'Passwords are not equal. Please, try again';
    }

}