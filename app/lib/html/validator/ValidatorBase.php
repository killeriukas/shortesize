<?php

namespace lib\html\validator;

use lib\data_structure\Dictionary;

abstract class ValidatorBase
{

    private $messages = array();

    protected function AddMessage($message)
    {
        $this->messages[] = $message;
    }

    public function HasMessages()
    {
        return !empty($this->messages);
    }

    public function GetMessages()
    {
        return $this->messages;
    }

    public function GetMessage($index = 0)
    {
        return $this->messages[$index];
    }

    public abstract function IsValid($value, Dictionary $values);
}