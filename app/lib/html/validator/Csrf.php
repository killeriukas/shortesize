<?php

namespace lib\html\validator;

use lib\data_structure\Dictionary;

class Csrf extends ValidatorBase
{

    public function IsValid($value, Dictionary $valueList)
    {
        $isValid = \lib\shared\Csrf::IsValid($value);

        if(!$isValid)
        {
            $this->AddMessage('Form has expired! Please, try again.');
            return false;
        }

        return true;
    }

}