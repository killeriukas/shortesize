<?php

namespace lib\html\form;

use lib\data_structure\Dictionary;
use lib\html\HtmlAttributes;
use lib\html\Tags;
use lib\shared\Csrf;

abstract class AForm
{
    const FORM_METHOD_DELETE = 'DELETE';
    const FORM_METHOD_PUT = 'PUT';
    const FORM_METHOD_POST = 'POST';
    const FORM_METHOD_GET = 'GET';

    private $method;
    private $action;

    protected $tags;

    protected function __construct($action, $method = AForm::FORM_METHOD_POST, HtmlAttributes $attributes = null)
    {
        $this->method = $method;
        $this->action = $action;
        $this->tags = new Tags();
    }

    final public function Render()
    {
        $this->tags->StartForm($this->method, $this->action);
        $this->CreateForm();
        $this->tags->CreateInputHidden(new HtmlAttributes(new Dictionary(array('name' => Csrf::CSRF_TOKEN, 'value' => Csrf::CreateCsrf()))));
        $this->tags->EndForm();
    }

    protected abstract function CreateForm();

    /*
     *     <form action="" method="post">
        First Name: <input type="text" name="firstName"><br>
        Last Name: <input type="text" name="lastName"><br>
        <input type="radio" name="accountType" value="bowner">Business Owner<br>
        <input type="radio" name="accountType" value="jseeker">Job Seeker<br>
        E-mail: <input type="text" name="email"><br>
        Password: <input type="password" name="password"><br>
        Repeat Password: <input type="password" name="rPassword"><br>
        <input type="checkbox" name="tandc">Terms & Conditions<br>
        <input type="submit" value="Apply" name="apply">
    </form>
     * */


/*
    private $parameters;

    private $elements = array();

    private static $defaults = array(
        'input' => '<input %TYPE% %NAME% %CLASS% %ID% %DEFAULT%>',
        'label' => '<label $FOR% %CLASS%>%CONTENT%</label>',
        'p' => '<p>$TITLE%</p>',
        'div_start' => '<div %CLASS%>',
        'div_end' => '</div>'
    );

    private static function ReplaceAttributes($template, $pattern, array $data, $htmlElemName)
    {
        $replacement = '';

        if (isset($data[$htmlElemName])) {
            $replacement = $htmlElemName . '="' . $data[$htmlElemName] . '"';
        }

        return str_replace($pattern, $replacement, $template);
    }

    private static function ReplaceText($template, $pattern, array $data, $htmlElemName)
    {
        $replacement = '';

        if (isset($data[$htmlElemName])) {
            $replacement = $data[$htmlElemName];
        }

        return str_replace($pattern, $replacement, $template);
    }

    private static function GetDefaults($htmlType, array $options)
    {
        if (array_key_exists($htmlType, self::$defaults)) {
            $htmlTemplate = self::$defaults[$htmlType];
            $htmlTemplate = AForm::ReplaceAttributes($htmlTemplate, '%TYPE%', $options, 'type');
            $htmlTemplate = AForm::ReplaceAttributes($htmlTemplate, '%NAME%', $options, 'name');
            $htmlTemplate = AForm::ReplaceAttributes($htmlTemplate, '%CLASS%', $options, 'class');
            $htmlTemplate = AForm::ReplaceAttributes($htmlTemplate, '%ID%', $options, 'id');
            $htmlTemplate = AForm::ReplaceText($htmlTemplate, '%DEFAULT%', $options, 'default');

            if (isset($options['label'])) {
                $labelInfo = $options['label'];

                $position = 'left';
                if (isset($labelInfo['position'])) {
                    $position = $labelInfo['position'];
                }

                $labelText = isset($labelInfo['title']) ? $labelInfo['title'] : '';
                switch ($position) {
                    case 'left':
                        $htmlTemplate = $labelText . ' '. $htmlTemplate;
                        break;
                    case 'right':
                        $htmlTemplate = $htmlTemplate . ' ' . $labelText;
                        break;
                    case 'top':

                        break;
                    default:
                        throw new \Exception('Label position is not valid: ' . $position);
                }

                $labelTemplate = self::$defaults['label'];
                $labelTemplate = AForm::ReplaceAttributes($labelTemplate, '%CLASS%', $labelInfo, 'class');
                $labelTemplate = AForm::ReplaceAttributes($labelTemplate, '%FOR%', $options, 'id');
                $htmlTemplate = str_replace('%CONTENT%', $htmlTemplate, $labelTemplate);
            }

            return $htmlTemplate;
        }

        throw new \Exception('This html type is not implemented yet: ' . $htmlType);
    }



    protected function StartPacking(array $parameters = array())
    {
        $this->AddElement('div_start', $parameters);
    }

    protected function EndPacking()
    {
        $this->AddElement('div_end', array());
    }

    protected function AddElement($htmlType, array $options = array())
    {
        $this->elements[] = array($htmlType, $options);
    }

    public function Render()
    {
        $this->InitializeElements();
        $attributes = Component::GetAttributes($this->parameters);
        echo '<form method="' . $this->method . '" ' . $attributes . '>';
        $this->PrintBody();
        echo '</form>';
    }

    private function PrintBody()
    {
        foreach ($this->elements as $element) {
            echo AForm::GetDefaults($element[0], $element[1]);
        }
    }

    protected abstract function InitializeElements();

    public function IsValid()
    {


        return true;
    }

    */

}