<?php

namespace lib\html\form;

class BlankFormValidator extends FormValidator
{

    public function IsBlank()
    {
        return true;
    }

    public function __construct()
    {
        parent::__construct(array());
    }

    protected function CreateValidators()
    {
        return null;
    }

    protected function CreateFilters()
    {
        return null;
    }
}