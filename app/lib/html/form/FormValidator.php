<?php

namespace lib\html\form;

use lib\data_structure\Dictionary;
use lib\html\filter\Filter;
use lib\html\filter\FilterBase;
use lib\shared\Functions;

abstract class FormValidator
{
    private $formData;

    private $formErrorMessages;

    public function __construct(array $data)
    {
        $this->formData = new Dictionary($data);
        $this->formErrorMessages = new Dictionary();
    }

    public function IsBlank()
    {
        return false;
    }

    public function GetData()
    {
        return $this->formData;
    }

    public function IsError($paramName)
    {
        $errorMessage = $this->formErrorMessages->Get($paramName);
        return !empty($errorMessage);
    }

    public function GetErrorMessage($paramName)
    {
        $escaped = Functions::EscapeHtml($this->formErrorMessages->Get($paramName));
        return $escaped;
    }

    public function GetEscaped($paramName)
    {
        $escaped = Functions::EscapeHtml($this->formData->Get($paramName));
        return $escaped;
    }

    protected abstract function CreateValidators();

    protected abstract function CreateFilters();

    private function CheckValidators(Validator $validator)
    {
        //skip when validator is not required
        if ($validator->IsRequired()) {
            $fieldName = $validator->GetFieldName();
            $fieldData = $this->formData->Get($fieldName);

            $validators = $validator->GetValidators();
            /***
             * @var \lib\html\validator\ValidatorBase $validatorBase
             */
            foreach ($validators as $validatorBase) {
                if (!$validatorBase->IsValid($fieldData, $this->formData)) {
                    $this->formErrorMessages->Add($fieldName, $validatorBase->GetMessage());
                    return false;
                }
            }
        }
        return true;
    }

    private function FilterValues(Filter $filter)
    {
        $fieldName = $filter->GetFieldName();
        $fieldData = $this->formData->Get($fieldName);

        $filters = $filter->GetFilters();
        /**
         * @var FilterBase $filterBase
         */
        foreach ($filters as $filterBase) {
            $filterBase->Filter($fieldData);
        }
    }

    public function IsValid()
    {
        $filtersArray = $this->CreateFilters();

        foreach ($filtersArray as $filter) {
            $this->FilterValues($filter);
        }

        if (!is_array($filtersArray)) {
            throw new \Exception('Cannot validate with no validators!');
        }

        $validatorsArray = $this->CreateValidators();

        if (!is_array($validatorsArray)) {
            throw new \Exception('Cannot validate with no validators!');
        }

        foreach ($validatorsArray as $validator) {
            if (!$this->CheckValidators($validator)) {
                return false;
            }
        }
        return true;
    }


}