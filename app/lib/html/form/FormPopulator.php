<?php

namespace lib\html\form;

use lib\shared\Dictionary;
use lib\shared\Functions;

class FormPopulator extends Dictionary
{

    public function __construct(array $formData = null)
    {
        if(!is_null($formData))
        {
            $this->Populate($formData);
        }
    }

    public function Populate(array $data)
    {
        foreach ($data as $paramName => $value) {
            $this->Add($paramName, $value);
        }
    }

    public function GetEscaped($paramName)
    {
        $escaped = Functions::EscapeHtml($this->Get($paramName));
        return $escaped;
    }

}