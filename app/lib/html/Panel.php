<?php

namespace lib\html;

use lib\data_structure\Dictionary;

abstract class Panel
{

    const PANEL_DEFAULT = 'panel-default';
    const PANEL_SAVED = 'panel-info';

    private $attributes;

    protected function __construct(HtmlAttributes $attributes = null)
    {
        $this->attributes = $attributes;
    }

    final public function Render()
    {
        if(null === $this->attributes) {
            $this->attributes = new HtmlAttributes(new Dictionary());
        }
        $this->attributes->AmendAttribute('class', 'panel ' . $this->GetPanelType());
        echo Tags::StartDiv($this->attributes);
        $this->PrintHeading();
        $this->PrintBody();
        $this->PrintFooter();
        echo Tags::EndDiv();
    }

    protected function GetPanelType()
    {
        return self::PANEL_DEFAULT;
    }

    protected function PrintHeading()
    {
        echo '<div class="panel-heading">';
        echo '<h2 class="panel-title">' . $this->GetHeading() . '</h2>';
        echo '</div>';
    }

    protected function GetHeading()
    {
        return 'Default Heading';
    }

    protected function PrintBody()
    {
        echo '<div class="panel-body">';
        $this->PrintBodyContent();
        echo '</div>';
    }

    protected function PrintBodyContent()
    {
        echo 'Empty Body Content';
    }

    protected function PrintFooter()
    {
        echo '<div class="panel-footer">';
        $this->PrintFooterContent();
        echo '</div>';
    }

    protected function PrintFooterContent()
    {
        echo 'Empty Footer Content';
    }

}