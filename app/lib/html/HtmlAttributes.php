<?php

namespace lib\html;

use lib\data_structure\Dictionary;

final class HtmlAttributes
{
    private $properties;

    public function __construct(Dictionary $properties = null)
    {
        if(null === $properties) {
            $this->properties = new Dictionary();
        } else {
            $this->Override($properties);
        }
    }

    public function Override(Dictionary $properties)
    {
        $this->properties = $properties;
    }

    public function Add($attribute, $value)
    {
        $this->properties->Add($attribute, $value);
    }

    public function AmendAttribute($attribute, $value)
    {
        $currentAttributes = $this->properties->Get($attribute);
        if(null === $currentAttributes) {
            $this->properties->Add($attribute, $value);
        } else {
            $this->properties->Set($attribute, $currentAttributes . ' ' . $value);
        }
    }

    public function AmendAttributes(HtmlAttributes $attributes)
    {
        foreach($attributes->properties as $key => $value) {
            $this->AmendAttribute($key, $value);
        }
    }

    public function GetAllAttributes()
    {
        $properties = '';
        foreach($this->properties as $key => $value) {
            $properties .= ($key . '="' . $value . '" ');
        }

        return $properties;
    }

}