<?php

namespace lib\database;

use lib\data_structure\Dictionary;

abstract class Query
{

    private $collectionName;

    private $query;

    private $fields;

    public function __construct($collectionName, array $query = array(), array $fields = array())
    {
        $this->collectionName = $collectionName;
        $this->query = $query;
        $this->fields = $fields;
    }

    final public function GetCollectionName()
    {
        return $this->collectionName;
    }

    final public function GetQuery()
    {
        return $this->query;
    }

    final public function GetFields()
    {
        return $this->fields;
    }

    public abstract function CreateModel(Dictionary $data);

}