<?php

namespace lib\database;

use lib\data_structure\Dictionary;
use lib\data_structure\GeneralList;
use lib\database\MongoDbHelper\MongoDbSingleton;

final class ModelLoader
{

//    private $database = null;
//
//    public function __construct(MongoDbSingleton $database)
//    {
//        $this->database = $database;
//    }

//    public function LoadData($collectionName, array $parameters)
//    {
//        if(is_null($parameters)) {
//            throw new \Exception('Cannot load the object with no search criteria!');
//        }
//
//        $collection = $this->database->GetCollection($collectionName);
//
//        $data = $collection->find($parameters);
//        return $data;
//    }
//
//    public function CreateModels(\MongoCursor $mongoCursor, $className)
//    {
//        if(!class_exists($className)) {
//            throw new \Exception("Class doesn't exist: " . $className);
//        }
//
//        if(is_null($mongoCursor)) {
//            throw new Exception('Passed cursor is null!');
//        }
//
//        $models = array();
//        if($mongoCursor->count() > 0) {
//            while($mongoCursor->hasNext()) {
//                $mongoCursor->next();
//                $model = new $className($mongoCursor->current());
//                $models[] = $model;
//            }
//        }
//
//        return $models;
//    }

    private static function LoadCollectionData(Query $query)
    {
        $collection = MongoDbSingleton::GetInstance()->GetCollection($query->GetCollectionName());
        $data = $collection->find($query->GetQuery(), $query->GetFields());

        if(empty($data) || 0 === $data->count()) {
            return null;
        }

        return $data;
    }

    public static function Load(Query $query)
    {
        $collectionData = self::LoadCollectionData($query);

        if(null === $collectionData) {
            return null;
        }

        $models = new GeneralList();
        while($collectionData->hasNext()) {
            $collectionData->next();
            $model = $query->CreateModel(new Dictionary($collectionData->current()));
            $models->Add($model);
        }

        return $models;
    }

    public static function LoadArranged(Query $query, $arrangeBy)
    {
        $collectionData = self::LoadCollectionData($query);

        if(null === $collectionData) {
            return null;
        }

        $models = new Dictionary();
        while($collectionData->hasNext()) {
            $collectionData->next();
            $curDictionaryData = new Dictionary($collectionData->current());
            $model = $query->CreateModel($curDictionaryData);

            $arrangedByValue = $curDictionaryData->Get($arrangeBy);
            if(null === $arrangedByValue) {
                var_dump($curDictionaryData);
                throw new \BadMethodCallException('SortBy [' . $arrangeBy . '] variable is not found in a current model.');
            }

            $models->Add($arrangedByValue, $model);
        }

        return $models;
    }

    public static function Delete(Query $query)
    {
        $collection = MongoDbSingleton::GetInstance()->GetCollection($query->GetCollectionName());
        $hasDeleted = $collection->remove($query->GetQuery(), $query->GetFields());

        //returns the whole array of information - do not need it.
        return $hasDeleted;
    }

}
