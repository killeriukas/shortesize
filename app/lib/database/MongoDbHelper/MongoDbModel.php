<?php

namespace lib\database\MongoDbHelper;

use lib\data_structure\Dictionary;
use lib\database\Model;

abstract class MongoDbModel extends Model
{

    /**
     * @var MongoDbSingleton
     */
    private $database = null;

    /**
     * @var string
     */
    private $collectionName = '';

    protected function __construct(MongoDbSingleton $database, $collectionName, Dictionary $data = null)
    {
        parent::__construct($data);

        if(is_null($database)) {
            throw new \Exception('Database connection cannot be null!');
        }

        $this->database = $database;

        if(!is_string($collectionName)) {
            throw new \Exception('Collection name cannot be empty!');
        }

        $this->collectionName = $collectionName;
    }

    final public function Exists()
    {
        $collection = $this->database->GetCollection($this->collectionName);

        $uniqueId = $this->GetUniquePair();

        $data = $collection->findOne($uniqueId);

        if(null !== $data) {
            return true;
        }

        return false;
    }

    protected abstract function GetUniquePair();

    public function Save()
    {
        $data = $this->PrepareToSave();
        $collection = $this->database->GetCollection($this->collectionName);

        if ($this->Exists()) {
            echo 'should update here!';
            //TODO: ADD UPDATE FUNCTIONALITY HERE
        } else {
            try {
                $collection->insert($data->ToArray());
            } catch(\Exception $exceptInfo) {
                echo 'Mongo Error: ' . $exceptInfo->getMessage();
                exit;
            }
        }
    }

//    public function MakeIndex(array $parameters)
//    {
//        $collection = $this->database->GetCollection($this->collectionName);
//
//        if(!isset($parameters['keys'])) {
//            throw new \Exception('Keys cannot be empty when setting the index!');
//        }
//
//        $keys = $parameters['keys'];
//
//        if(!isset($parameters['options'])) {
//            throw new \Exception('Options cannot be empty when setting the index!');
//        }
//
//        $options = $parameters['options'];
//
//        $collection->ensureIndex($keys, $options);
//    }

}
