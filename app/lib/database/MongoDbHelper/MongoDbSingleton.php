<?php

namespace lib\database\MongoDbHelper;

use lib\data_structure\GeneralList;

final class MongoDbSingleton
{

    /***
     * @var MongoDbSingleton
     */
    private static $instance = null;


    /***
     * @var \MongoClient
     */
    private $mongoClient = null;

    /***
     * @var \MongoDB
     */
    private $database = null;

    private function __construct(array $parameters)
    {
        $server = $parameters['server'];
        $options = $parameters['options'];
        $this->mongoClient = new \MongoClient($server, $options);
        $this->database = $this->mongoClient->selectDB($parameters['dbName']);
    }

    public static function GetInstance(array $parameters = null)
    {
        if (is_null(self::$instance)) {
            if (is_null($parameters)) {
                throw new \InvalidArgumentException('Parameters cannot be null when calling GetInstance() method for the very first time!');
            }
            self::$instance = new MongoDbSingleton($parameters);
        }
        return self::$instance;
    }

    /**
     * @param string $collectionName
     * @return \MongoCollection
     */
    public function GetCollection($collectionName)
    {
        $selectedCollection = $this->database->selectCollection($collectionName);
        return $selectedCollection;
    }

    public function GetCollections($includeSystemCollections = false)
    {
        $collectionNames = new GeneralList($this->database->getCollectionNames($includeSystemCollections));
        return $collectionNames;
    }


}