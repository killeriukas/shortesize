<?php

namespace lib\database;

final class MiniModel
{

    private $dirty;
    private $value;

    public function __construct()
    {
        $this->dirty = false;
    }

    public function SetValue($value)
    {
        if($this->value !== $value) {
            $this->value = $value;
            $this->dirty = true;
        }
    }

    public function GetValue()
    {
        return $this->value;
    }

    public function Save()
    {
        $this->dirty = false;
    }

    public function IsDirty()
    {
        return $this->dirty;
    }
}