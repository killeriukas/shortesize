<?php

namespace lib\database;

use lib\data_structure\Dictionary;

abstract class Model
{

    private $properties;

    protected function __construct(Dictionary $data = null)
    {
        $this->properties = new Dictionary();

        if(null !== $data) {
            foreach($data as $key => $value) {
                $this->SetVariable($key, $value);
            }
        }

        //deprecated
        if(null !== $data) {
            $this->Populate($data);
        }
    }

    final protected function SetVariable($key, $value)
    {
        if($this->properties->Constains($key)) {
            /* @var $miniModel MiniModel */
            $miniModel = $this->properties->Get($key);
            $miniModel->SetValue($value);
        } else {
            $miniModel = new MiniModel();
            $miniModel->SetValue($value);
            $this->properties->Add($key, $miniModel);
        }
    }

    final protected function GetVariable($key)
    {
        if($this->properties->Constains($key)) {
            /* @var $miniModel MiniModel */
            $miniModel = $this->properties->Get($key);
            return $miniModel->GetValue();
        }
        throw new \InvalidArgumentException('Get variable can never be void, because variable always exists!');
    }

    final public function Populate(Dictionary $data)
    {
        foreach($data as $key => $value) {
            if(property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    final protected function PrepareToSave()
    {
        $savedData = new Dictionary();

        /* @var $miniModel MiniModel */
        foreach($this->properties as $key => $miniModel) {
            if($miniModel->IsDirty()) {
                $savedData->Add($key, $miniModel->GetValue());

                //TODO: think about SAVE!!!!! cuz if other class overrides SAVE method and calls this and never saves, it will be a false assumption and the flags will be reset
                $miniModel->Save();
            }
        }

        return $savedData;
    }

//    final protected function PrepareToSave()
//    {
//        return get_object_vars($this);
//    }

    public abstract function Exists();
    public abstract function Save();

    //public abstract function MakeIndex(array $parameters);
}