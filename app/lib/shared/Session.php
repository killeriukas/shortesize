<?php

namespace lib\shared;

final class Session
{

    const LAST_TIME_UPDATED_KEY = 'LAST_TIME_UPDATED_KEY';

    /**
     * how long user can leave their machine passive without being kicked off
     */
    const CLEAR_SESSION_PASSIVE_TIME_SEC = 3600;

    public static function GetInstance()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new self();
        }
        return $instance;
    }

    private function __construct()
    {

    }

    public function StartSession()
    {
        //session_cache_expire(false);
        //session_cache_limiter(false);
        $sessionStarted = session_start();
        if ($sessionStarted === false) {
            throw new \Exception('Error: Session was not started!');
        }

        //clean session if session has timed out
        $this->CleanSessionIfDead();
    }

    public function ForceEndSession()
    {
        $this->CleanSession();
        $sessionDestroyed = session_destroy();
        if ($sessionDestroyed === false) {
            //TODO: die silently
        }
    }

    private function CleanSessionIfDead()
    {
        if (!$this->IsSessionAlive()) {
            $this->CleanSession();
        }
        $this->SetKey(self::LAST_TIME_UPDATED_KEY, time());
    }

    public function Set($key, $information)
    {
        //every time you interact with session, check if you need to clear it
        $this->CleanSessionIfDead();

        $keyExists = $this->KeyExists($key);

        if ($keyExists === true) {
            throw new \Exception('Error: Trying to override the same key: ' . $key . '!');
        }

        $this->SetKey($key, $information);
    }

    public function Get($key)
    {
        //every time you interact with session, check if you need to clear it
        $this->CleanSessionIfDead();

        $keyExists = $this->KeyExists($key);

        if ($keyExists === false) {
            return null;
        }

        return $this->GetValue($key);
    }

    public function Remove($key)
    {
        $this->RemoveKey($key);
    }

    private function CleanSession()
    {
        session_unset();
    }

    private function KeyExists($key)
    {
        return isset($_SESSION[$key]);
    }

    private function RemoveKey($key)
    {
        $exists = $this->KeyExists($key);
        if ($exists) {
            unset($_SESSION[$key]);
        }
    }

    private function SetKey($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    private function GetValue($key)
    {
        if ($this->KeyExists($key)) {
            return $_SESSION[$key];
        }

        return null;
    }

    private function IsSessionAlive()
    {
        if ($this->KeyExists(self::LAST_TIME_UPDATED_KEY)) {
            $deathTime = $this->GetValue(self::LAST_TIME_UPDATED_KEY) + self::CLEAR_SESSION_PASSIVE_TIME_SEC;
            $now = time();
            if ($deathTime > $now) {
                return true;
            }
        }

        return false;
    }

}