<?php

namespace lib\shared;

final class Csrf
{
    const CSRF_TOKEN = 'CSRF_TOKEN';
    const CSRF_TIMEOUT_SEC = 300;

    public static function CreateCsrf()
    {
        Csrf::RemoveCsrf();

        $code = Functions::CreateToken();

        $csrf = array(
            'time' => time(),
            'csrf' => $code
        );

        Session::GetInstance()->Set(Csrf::CSRF_TOKEN, $csrf);
        return $code;
    }

    public static function IsValid($csrf)
    {
        $cypher = Session::GetInstance()->Get(Csrf::CSRF_TOKEN);

        if(!is_null($cypher))
        {
            if($cypher['time'] + Csrf::CSRF_TIMEOUT_SEC > time())
            {
                if($cypher['csrf'] === $csrf)
                {
                    Csrf::RemoveCsrf();
                    return true;
                }
            }
            else
            {
                Csrf::RemoveCsrf();
            }
        }

        return false;
    }

    public static function RemoveCsrf()
    {
        Session::GetInstance()->Remove(Csrf::CSRF_TOKEN);
    }
}