<?php

namespace lib\slim;

use Slim\Slim;
use website\controllers\auth\user\UserService;
use website\html\account\HtmlViewFactory;

abstract class AController
{

    protected $app = null;
    protected $user = null;
    protected $html = null;

    public function __construct($title = '')
    {
        $this->app = Slim::getInstance();
        $this->user = UserService::GetInstance()->GetUser();
        //$this->html = HtmlViewFactory::CreateHtmlViewByType($this->user->GetAccountType(), PROJECT_NAME . ' | ' . $title);
    }


}