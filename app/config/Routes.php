<?php

define('WEB_MAIN_GET', 'MAIN_GET');

//log in/out
define('WEB_LOGIN_GET', 'LOGIN_GET');
define('WEB_LOGIN_POST', 'LOGIN_POST');
define('WEB_LOGOUT_GET', 'LOGOUT_GET');

//register
define('WEB_REGISTER_GET', 'REGISTER_GET');
define('WEB_REGISTER_POST', 'REGISTER_POST');

//registered users account
define('WEB_MY_ACCOUNT_GET', 'MY_ACCOUNT_GET');

//admin
define('WEB_DB_GET', 'DB_GET');
define('WEB_COLLECTION_GET', 'COLLECTION_GET');

//business owner
define('WEB_JOB_LISTINGS_GET', 'JOB_LISTINGS_GET');
define('WEB_JOB_LISTING_CREATE_GET', 'JOB_LISTING_CREATE_GET');
define('WEB_JOB_LISTING_CREATE_POST', 'JOB_LISTING_CREATE_POST');

//job seeker
define('WEB_JOB_SAVED_LIST_GET', 'JOB_SAVED_LIST_GET');
define('WEB_JOB_APPLIED_LIST_GET', 'JOB_APPLIED_LIST_GET');
define('WEB_JOB_SAVE_POST', 'JOB_SAVE_POST');
define('WEB_JOB_DELETE_DELETE', 'JOB_DELETE_DELETE');
define('WEB_JOB_APPLY_POST', 'JOB_APPLY_POST');

//errors
define('WEB_PAGE_NOT_FOUND', 'ERROR_404');
define('WEB_ERROR_ACCESS_DENIED', 'ERROR_401');

return array(
    WEB_LOGIN_GET => array(
        'url' => '/auth/login',
        'controller' => 'website\controllers\auth\LoginController@IndexAction',
        'request_type' => 'get'
    ),
    WEB_LOGIN_POST => array(
        'url' => '/auth/login',
        'controller' => 'website\controllers\auth\LoginController@LoginAction',
        'request_type' => 'post'
    ),
    WEB_MY_ACCOUNT_GET => array(
        'url' => '/myaccount',
        'controller' => 'website\controllers\account\AccountController@IndexAction',
        'request_type' => 'get'
    ),
    WEB_ERROR_ACCESS_DENIED => array(
        'url' => '/error/accessdenied',
        'controller' => 'website\controllers\error\ErrorController@AccessDeniedAction',
        'request_type' => 'get'
    ),
    WEB_LOGOUT_GET => array(
        'url' => '/auth/logout',
        'controller' => 'website\controllers\auth\LogoutController@IndexAction',
        'request_type' => 'get'
    ),
    WEB_MAIN_GET => array(
        'url' => '/',
        'controller' => 'website\controllers\main\IndexController@IndexAction',
        'request_type' => 'get'
    ),
    WEB_DB_GET => array(
        'url' => '/db',
        'controller' => 'website\controllers\account\admin\DatabaseController@IndexAction',
        'request_type' => 'get'
    ),
    WEB_COLLECTION_GET => array(
        'url' => '/db/collection/:collection_id/get',
        'controller' => 'website\controllers\account\admin\CollectionController@CollectionGetAction',
        'request_type' => 'get'
    ),
    WEB_REGISTER_GET => array(
        'url' => '/auth/register',
        'controller' => 'website\controllers\auth\RegisterController@IndexAction',
        'request_type' => 'get'
    ),
    WEB_REGISTER_POST => array(
        'url' => '/auth/register',
        'controller' => 'website\controllers\auth\RegisterController@RegisterAction',
        'request_type' => 'post'
    ),
    WEB_JOB_LISTINGS_GET => array(
        'url' => '/job/listings',
        'controller' => 'website\controllers\account\job\listings\ListingsController@IndexAction',
        'request_type' => 'get'
    ),
    WEB_JOB_LISTING_CREATE_GET => array(
        'url' => '/job/listings/create',
        'controller' => 'website\controllers\account\job\listings\ListingsController@CreateListingAction',
        'request_type' => 'get'
    ),
    WEB_JOB_LISTING_CREATE_POST => array(
        'url' => '/job/listings/create',
        'controller' => 'website\controllers\account\job\listings\ListingsController@SaveListingAction',
        'request_type' => 'post'
    ),
    WEB_JOB_SAVED_LIST_GET => array(
        'url' => '/job/saved',
        'controller' => 'website\controllers\account\job\JobController@GetSavedJobsAction',
        'request_type' => 'get'
    ),
    WEB_JOB_APPLIED_LIST_GET => array(
        'url' => '/job/applied',
        'controller' => 'website\controllers\account\job\JobController@GetAppliedJobsAction',
        'request_type' => 'get'
    ),
    WEB_JOB_SAVE_POST => array(
        'url' => '/job/save',
        'controller' => 'website\controllers\account\job\JobController@SaveJobAction',
        'request_type' => 'post'
    ),
    WEB_JOB_DELETE_DELETE => array(
        'url' => '/job/delete',
        'controller' => 'website\controllers\account\job\JobController@DeleteJobAction',
        'request_type' => 'delete'
    ),
    WEB_JOB_APPLY_POST => array(
        'url' => '/job/apply',
        'controller' => 'website\controllers\account\job\JobController@ApplyJobAction',
        'request_type' => 'post'
    )


);