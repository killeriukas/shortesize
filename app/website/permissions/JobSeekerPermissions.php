<?php

$permissions = require 'GuestPermissions.php';

//account
$permissions[WEB_MY_ACCOUNT_GET] = true;
$permissions[WEB_LOGOUT_GET] = true;

//jobs related
$permissions[WEB_JOB_SAVED_LIST_GET] = true;
$permissions[WEB_JOB_APPLIED_LIST_GET] = true;
$permissions[WEB_JOB_SAVE_POST] = true;
$permissions[WEB_JOB_DELETE_DELETE] = true;
$permissions[WEB_JOB_APPLY_POST] = true;

return $permissions;