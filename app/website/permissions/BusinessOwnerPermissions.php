<?php

$permissions = require 'GuestPermissions.php';
$permissions[WEB_MY_ACCOUNT_GET] = true;
$permissions[WEB_LOGOUT_GET] = true;
$permissions[WEB_JOB_LISTINGS_GET] = true;
$permissions[WEB_JOB_LISTING_CREATE_GET] = true;
$permissions[WEB_JOB_LISTING_CREATE_POST] = true;

return $permissions;