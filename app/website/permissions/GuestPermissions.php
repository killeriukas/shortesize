<?php

return array(
    WEB_MAIN_GET => true,

    //errors
    WEB_ERROR_ACCESS_DENIED => true,

    //registered users stuff
    WEB_MY_ACCOUNT_GET => false,
    WEB_LOGOUT_GET => false,

    //admin
    WEB_DB_GET => false,
    WEB_COLLECTION_GET => false,

    //business owner
    WEB_JOB_LISTINGS_GET => false,
    WEB_JOB_LISTING_CREATE_GET => false,
    WEB_JOB_LISTING_CREATE_POST => false,

    //job seeker
    WEB_JOB_SAVED_LIST_GET => false,
    WEB_JOB_APPLIED_LIST_GET => false,
    WEB_JOB_SAVE_POST => false,
    WEB_JOB_DELETE_DELETE => false,
    WEB_JOB_APPLY_POST => false,

    //login
    WEB_LOGIN_GET => true,
    WEB_LOGIN_POST => true,

    //register
    WEB_REGISTER_GET => true,
    WEB_REGISTER_POST => true
);