<?php

use lib\html\table\Table;
use lib\html\HtmlAttributes;
use lib\data_structure\Dictionary;
use lib\html\Tags;

/* @var $html \website\html\GeneralHtml */
$html = $data['html'];

$html->StartHtml($data);
?>
    <div class="container-fluid">
        <h1>Jobs Applied</h1>
    </div>
    <div class="container-fluid">
        <?php

        /* @var $jobModels \lib\data_structure\Dictionary */
        $jobModels = $data['jobModels'];

        if(null === $jobModels) {
            \lib\html\Message::Information('Applied jobs list is empty!');
        } else {
            $savedJobsTable = new Table(new HtmlAttributes(new Dictionary(array('class' => 'table'))));
            $savedJobsTable->AddRow();

            $columnHeaders = new \lib\data_structure\GeneralList();
            $columnHeaders->Add('Job Title');
            $columnHeaders->Add('Location');
            $columnHeaders->Add('Salary');
            $columnHeaders->Add('Time Saved');
            $columnHeaders->Add('Delete');

            $savedJobsTable->AddHeader($columnHeaders);

            $deleteSpanAttributes = new Dictionary();
            $deleteSpanAttributes->Add('class', 'glyphicon glyphicon-trash');
            $deleteSpan = Tags::CreateSpan('', new HtmlAttributes($deleteSpanAttributes));

            $deleteButtonAttributes = new Dictionary();
            $deleteButtonAttributes->Add('class', 'btn btn-danger ajax_delete');
            $deleteButtonAttributes->Add('title', 'Delete');
            $deleteButtonAttributes->Add('id', 'Empty');

            /*  @var $value \website\models\job\JobSavedModel */
            foreach($jobModels as $key => $value) {
                $savedJobsTable->AddRow();

                $columnData = new \lib\data_structure\GeneralList();
                $columnData->Add($value->GetTitle());
                $columnData->Add($value->GetLocation());
                $columnData->Add($value->GetSalary());

                $secondsUtc = $value->GetSaveDate()->sec;
                $now = time() - $secondsUtc;

                $columnData->Add(\lib\shared\Functions::FormatTimeSince($now));

                //create delete button
                $deleteButtonAttributes->Set('id', $value->GetJobId());
                $deleteButton = Tags::CreateButtonDefault($deleteSpan, new HtmlAttributes($deleteButtonAttributes));

                $columnData->Add($deleteButton);

                $savedJobsTable->AddColumn($columnData);
            }

            $savedJobsTable->Render();
        }

        ?>
    </div>
    <script>
        $(document).ready(function () {
            $('.ajax_delete').click(function (e) {
                e.preventDefault();
                var curTarget = $(e.currentTarget);
                $(curTarget).addClass('disabled');

                var jobId = $(curTarget).attr('id');

                AddAjax('delete',
                    '/job/delete',
                    'job_id=' + jobId,
                    function (info) {
                        $(curTarget).parents('tr').remove();
                        console.log(info);
                    },
                    function (jqXHR, textStatus, errorThrown) {
                        alert(errorThrown);
                    });
            });
        });
    </script>
<?php
$html->EndHtml();