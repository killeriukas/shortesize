<?php

/* @var $html \website\html\GeneralHtml */
$html = $data['html'];

$html->StartHtml($data);

use lib\html\table\Table;

use \lib\html\HtmlAttributes;
use \lib\data_structure\Dictionary;

?>
    <div class="container-fluid">
        <h1>Job Listings</h1>
        <?php
        /* @var $jobModels lib\data_structure\GeneralList */
        $jobModels = $data['jobModels'];

        if (empty($jobModels)) {
            echo 'empty!';
        } else {
            $jobListingsTable = new Table(array('class' => 'table table-striped'));
            $jobListingsTable->AddRow();
            $jobListingsTable->AddHeader(
                array(
                    '#',
                    'Title',
                    'Location',
                    'Salary, $',
                    '# Applied',
                    'Last updated, UTC',
                    'Created, UTC',
                    'Created by',
                    'Edit',
                    'Active',
                    'Remove')
            );

            for ($i = 0; $i < $jobModels->Count(); ++$i) {

                /* @var $job website\models\account\JobModel */
                $job = $jobModels->Get($i);
                $jobListingsTable->AddRow();
                $jobListingsTable->AddColumn(
                    array(
                        ($i + 1),
                        '<a href="/job/listings/' . $job->GetGuid() . '/get">' . $job->GetTitle() . '</a>',
                        $job->GetLocation(),
                        $job->GetSalary(),
                        $job->GetPeopleApplied(),
                        $job->GetLastUpdated_Readable(),
                        $job->GetCreated_Readable(),
                        $job->GetCreator(),
                        'Edit Btn',
                        $job->IsActivated() ? 'Yes' : 'No',
                        'Remove Btn')
                );
            }
            $jobListingsTable->Render();
        }


        $spanAttributes = new HtmlAttributes(new Dictionary(array('class' => 'glyphicon glyphicon-plus')));
        $createSpan = $html->GetTags()->CreateSpan('', $spanAttributes);

        $linkAttributes = new HtmlAttributes(new Dictionary(array('class' => 'btn btn-primary')));

        echo $html->GetTags()->CreateLink($createSpan . ' Create New', '/job/listings/create', $linkAttributes);
        ?>

    </div>
<?php
$html->EndHtml();
