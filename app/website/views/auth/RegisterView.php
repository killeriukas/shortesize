<?php
$app = \Slim\Slim::getInstance();

/***
 * @var \lib\html\form\FormValidator $formValidator
 */

$formValidator = $data['formValidator'];

function PrintError(\lib\html\form\FormValidator $formValidator, $fieldName)
{
    if ($formValidator->IsError($fieldName)) {
        echo '<span class="help-block">' . $formValidator->GetErrorMessage($fieldName) . '</span>';
    }
}

function HasError(\lib\html\form\FormValidator $formValidator, $fieldName)
{
    if ($formValidator->IsError($fieldName)) {
        echo 'has-error';
    }
}

function IsChecked(\lib\html\form\FormValidator $formValidator, $fieldName, $fieldValue, $default)
{
    $data = $formValidator->GetEscaped($fieldName);
    if ($data === $fieldValue || $default) {
        echo 'checked';
    }
}

function AddRadio(\lib\html\form\FormValidator $formValidator, $name, $label, $radioValue, $onClickFunction, $isChecked = false)
{
    ?>
    <label class="radio-inline">
        <input onclick="<?php echo $onClickFunction; ?>" type="radio" name="<?php echo $name; ?>"
               value="<?php echo $radioValue; ?>" <?php IsChecked($formValidator, $name, $radioValue, $isChecked); ?>>
        <?php echo $label; ?>
    </label>
<?php
}

function AddFormGroup(\lib\html\form\FormValidator $formValidator, $name, $label, $placeHolder)
{
    ?>
    <div class="form-group <?php HasError($formValidator, $name); ?>">
        <label for="<?php echo $name; ?>" class="col-sm-4 control-label"><?php echo $label; ?></label>

        <div class="col-sm-3">
            <input type="text" id="<?php echo $name; ?>" name="<?php echo $name; ?>" class="form-control"
                   placeholder="<?php echo $placeHolder; ?>" value="<?php echo $formValidator->GetEscaped($name); ?>">
            <?php PrintError($formValidator, $name); ?>
        </div>
    </div>
<?php
}

function AddPasswordText(\lib\html\form\FormValidator $formValidator, $name, $label, $placeHolder)
{
    ?>
    <div class="form-group <?php HasError($formValidator, $name); ?>">
        <label for="<?php echo $name; ?>" class="col-sm-4 control-label"><?php echo $label; ?></label>

        <div class="col-sm-3">
            <input type="password" id="<?php echo $name; ?>" name="<?php echo $name; ?>" class="form-control"
                   placeholder="<?php echo $placeHolder; ?>" value="">
            <?php PrintError($formValidator, $name); ?>
        </div>
    </div>
<?php
}

$html = new \website\html\account\GuestHtml('Registration Page');
$html->StartHtml();
?>
    <div class="container">
        <div class="row">
            <div class="page-header">
                <h1>Be more than just a Traveler. Join us</h1>
            </div>
        </div>
        <form class="form-horizontal" method="post">
            <div class="form-group">
                <label class="col-sm-4 control-label">Account Type</label>

                <div class="col-sm-5">
                    <?php
                    $radioButtonName = 'accountType';
                    AddRadio($formValidator, $radioButtonName, 'Job Seeker', \website\models\users\UserFactory::ACCOUNT_TYPE_JOB_SEEKER, 'UserSelected()', $formValidator->IsBlank());
                    AddRadio($formValidator, $radioButtonName, 'Business Owner', \website\models\users\UserFactory::ACCOUNT_TYPE_BUSINESS_OWNER, 'BSSelected()');
                    if ($app->getMode() === DEVELOPMENT) {
                        AddRadio($formValidator, $radioButtonName, 'Administrator', \website\models\users\UserFactory::ACCOUNT_TYPE_ADMIN, 'UserSelected()');
                        AddRadio($formValidator, $radioButtonName, 'Moderator', \website\models\users\UserFactory::ACCOUNT_TYPE_MODERATOR, 'UserSelected()');
                    }
                    ?>
                </div>
            </div>

            <?php //job seeker account/ administrator/ moderator ?>
            <div id="user_reg">
                <?php
                AddFormGroup($formValidator, 'fname', 'First Name', 'Name');
                AddFormGroup($formValidator, 'lname', 'Last Name', 'Surname');
                ?>
            </div>

            <?php //business owner ?>
            <div id="bs_reg">
                <?php
                AddFormGroup($formValidator, 'cname', 'Company Name', 'Example Ltd');
                ?>
            </div>

            <?php
            //everyone
            AddFormGroup($formValidator, 'email', 'E-mail', 'example@example.com');
            AddPasswordText($formValidator, 'password', 'Password', 'Password');
            AddPasswordText($formValidator, 'rpassword', 'Repeat Password', 'Repeat Password');
            ?>
            <div class="form-group <?php HasError($formValidator, 'tandc'); ?>">
                <div class="col-sm-offset-4 col-sm-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="tandc"> Terms &amp; Conditions <a href="#">(more)</a>
                        </label>
                    </div>
                    <?php PrintError($formValidator, 'tandc'); ?>
                </div>
            </div>
            <div class="form-group <?php HasError($formValidator, 'csrf_token'); ?>">
                <div class="col-sm-offset-4 col-sm-3">
                    <input type="submit" value="Cancel" name="cancel" class="btn btn-default">
                    <input type="submit" value="Register" name="register" class="btn btn-success">
                    <?php PrintError($formValidator, 'csrf_token'); ?>
                </div>
            </div>
            <input type="hidden" value="<?php echo \lib\shared\Csrf::CreateCsrf(); ?>" name="csrf_token">
        </form>
    </div>
    <script>
        function UserSelected() {
            $('#bs_reg').hide();
            $('#user_reg').show();
        }

        function BSSelected() {
            $('#user_reg').hide();
            $('#bs_reg').show();
        }

        $(document).ready(function () {
            <?php
                $accountType = $formValidator->GetEscaped('accountType');
                if($formValidator->IsBlank() || $accountType === \website\models\user\UserFactory::ACCOUNT_TYPE_JOB_SEEKER)
                {
                    echo 'UserSelected();';
                }
                else
                {
                    echo 'BSSelected();';
                }
            ?>
        });
    </script>
<?php
$html->EndHtml();