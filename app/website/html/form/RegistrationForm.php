<?php

namespace website\html\form;

use lib\html\form\AForm;

class RegistrationAForm extends AForm
{

    /*
 *     <form action="" method="post">
    First Name: <input type="text" name="firstName"><br>
    Last Name: <input type="text" name="lastName"><br>
    <input type="radio" name="accountType" value="bowner">Business Owner<br>
    <input type="radio" name="accountType" value="jseeker">Job Seeker<br>
    E-mail: <input type="text" name="email"><br>
    Password: <input type="password" name="password"><br>
    Repeat Password: <input type="password" name="rPassword"><br>
    <input type="checkbox" name="tandc">Terms & Conditions<br>
    <input type="submit" value="Apply" name="apply">
</form>
 * */

    public function __construct()
    {
        parent::__construct(AForm::POST, array('class' => 'form-horizontal'));
    }

    protected function InitializeElements()
    {
        $this->StartPacking(array(
            'class' => 'form-group'
        ));

        //radio options for account type
        $this->AddElement('input',
            array('type' => 'radio',
                'name' => 'accountType',
                'value' => 'jseeker',
                'default' => 'checked',
                'label' => array('title' => 'Job Seeker', 'position' => 'right', 'class' => 'radio-inline')
            )
        );

        $this->AddElement('input',
            array('type' => 'radio',
                'name' => 'accountType',
                'value' => 'bowner',
                'label' => array('title' => 'Business Owner', 'position' => 'right', 'class' => 'radio-inline')
            )
        );

        $this->AddElement('input',
            array('type' => 'radio',
                'name' => 'accountType',
                'value' => 'admin',
                'label' => array('title' => 'Administrator', 'position' => 'right', 'class' => 'radio-inline')
            )
        );

        $this->EndPacking();

        $this->StartPacking(array(
            'class' => 'form-group'
        ));

        $this->AddElement('input',
            array('type' => 'text',
                'name' => 'firstName',
                'class' => 'form-control',
                'label' => array(
                    'title' => 'First Name:'//,
                //    'class' => 'none'
                )
            ));

        $this->EndPacking();

        $this->AddElement('input',
            array('type' => 'text',
                'name' => 'lastName',
                'label' => array(
                    'title' => 'Last Name:',
                    'class' => 'none'
                )
            ));
    }
}