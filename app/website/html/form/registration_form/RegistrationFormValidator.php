<?php

namespace website\html\form\registration_form;

use lib\html\filter\Filter;
use lib\html\filter\FilterTrim;
use lib\html\form\FormValidator;
use lib\html\form\Validator;
use lib\html\validator\AlphaNumeric;
use lib\html\validator\CheckedTandC;
use lib\html\validator\Csrf;
use lib\html\validator\Email;
use lib\html\validator\MatchPassword;

abstract class RegistrationFormValidator extends FormValidator
{

    protected function CreateValidators()
    {
        $validators = array(
            new Validator('email', true, array(new Email()), array()),
            new Validator('password', true, array(new AlphaNumeric(), new MatchPassword(array('rpassword'))), array()),
            new Validator('rpassword', true, array(new AlphaNumeric(), new MatchPassword(array('password'))), array()),
            new Validator('tandc', true, array(new CheckedTandC()), array()),
            new Validator('csrf_token', true, array(new Csrf()), array())
        );

        return $validators;
    }

    protected function CreateFilters()
    {
        $filters = array(
            new Filter('email', array(new FilterTrim()))
        );

        return $filters;
    }
}