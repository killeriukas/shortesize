<?php

namespace website\html\form\registration_form;

use website\models\users\UserFactory;

final class RegistrationValidatorFactory
{

    public static function CreateValidator($type, $formData)
    {
        $validator = null;
        switch ($type) {
            case UserFactory::ACCOUNT_TYPE_JOB_SEEKER:
                $validator = new JobSeekerRegistrationFormValidator($formData);
                break;
            case UserFactory::ACCOUNT_TYPE_ADMIN:
                $validator = new AdminRegistrationFormValidator($formData);
                break;
            case UserFactory::ACCOUNT_TYPE_BUSINESS_OWNER:
                $validator = new BSRegistrationFormValidator($formData);
                break;
//            case UserFactory::ACCOUNT_TYPE_MODERATOR:
//
//                break;
            default:
                throw new \Exception("Validator of type " . $type . " doesn't exist in the current context!");
        }

        return $validator;
    }

}