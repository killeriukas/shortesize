<?php

namespace website\html\form\registration_form;

use lib\html\form\Validator;
use lib\html\validator\AlphaNumeric;
use lib\html\validator\ValidatorNotEmpty;

class BSRegistrationFormValidator extends RegistrationFormValidator
{

    protected function CreateValidators()
    {
        $validators = parent::CreateValidators();
        $validators[] = new Validator('cname', true, array(new ValidatorNotEmpty(), new AlphaNumeric()), array());
        return $validators;
    }

    protected function CreateFilters()
    {
        $filters = parent::CreateFilters();



        return $filters;
    }
}