<?php

namespace website\html\account;

use website\models\users\UserFactory;

final class HtmlViewFactory
{

    public static function CreateHtmlViewByType($type, $title)
    {
        $htmlView = null;
        switch ($type) {
            case UserFactory::ACCOUNT_TYPE_ADMIN:
                $htmlView = new MyAccountAdminHtml($title);
                break;
            case UserFactory::ACCOUNT_TYPE_BUSINESS_OWNER:
                $htmlView = new MyAccountBusinessOwnerHtml($title);
                break;
            case UserFactory::ACCOUNT_TYPE_GUEST:
                $htmlView = new GuestHtml($title);
                break;
            case UserFactory::ACCOUNT_TYPE_JOB_SEEKER:
                $htmlView = new JobSeekerHtml($title);
                break;
            default:
                throw new \Exception('Type provided does not exist ' . $type);
                break;
        }
        return $htmlView;
    }

}