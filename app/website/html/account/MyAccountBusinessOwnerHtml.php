<?php

namespace website\html\account;

class MyAccountBusinessOwnerHtml extends GuestHtml
{

    protected function PrintTopBody($parameters)
    {
        /* @var $user \website\models\users\UserModel */
        $user = $parameters['user'];

        ?>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="nav navbar-header">
                    <a class="navbar-brand" href="/">Welcome, <?php echo $user->GetUserName(); ?></a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a href="/myaccount">My Account</a></li>
                    <li><a href="/job/listings">Job Listings</a></li>
                    <li><a href="/job/requests">Job Requests</a></li>
                    <li><a href="/faq">FAQ</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/auth/logout"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Log Out</a></li>
                </ul>
            </div>
        </nav>
        <?php
    }
}
