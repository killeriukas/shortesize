<?php

namespace website\html;

use lib\html\Html;

class GeneralHtml extends Html {

    protected function PrintLinks() {
        ?>
        <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <?php
    }

    protected function PrintMetas() {
        ?>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php
    }

    protected function PrintBottomBody($parameters) {

    }

    protected function PrintTopBody($parameters) {
        
    }

    protected function PrintTopScripts()
    {
        ?>
        <script src="/js/jquery-1.11.3.js"></script>
        <script src="/js/Ajax.js"></script>
        <?php
    }

    protected function PrintBottomScripts()
    {

    }
}
