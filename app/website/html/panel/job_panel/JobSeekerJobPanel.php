<?php

namespace website\html\panel\job_panel;

use lib\data_structure\Dictionary;
use website\html\panel\JobPanel;
use website\models\account\JobModel;

final class JobSeekerJobPanel extends JobPanel
{

    private $panelType;

    public function __construct(JobModel $jobModel)
    {
        parent::__construct($jobModel);
        $this->panelType = self::PANEL_DEFAULT;
    }

    public function Update(Dictionary $savedJobs)
    {
        $value = $savedJobs->Get($this->jobModel->GetGuid());

        if(null === $value) {
            return;
        }

        $this->panelType = self::PANEL_SAVED;

        //TODO: add more handling, not just for saved jobs, but for applied jobs etc.....
    }

    protected function GetPanelType()
    {
        return $this->panelType;
    }

}