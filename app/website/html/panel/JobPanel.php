<?php

namespace website\html\panel;

use lib\data_structure\Dictionary;
use lib\html\HtmlAttributes;
use lib\html\Panel;
use lib\html\Tags;
use website\controllers\auth\user\UserService;
use website\models\account\JobModel;
use website\models\users\UserModel;

class JobPanel extends Panel
{

    /**
     * @var JobModel
     */
    protected $jobModel = null;

    public function __construct(JobModel $jobModel)
    {
        parent::__construct();

        $this->jobModel = $jobModel;
    }

    final protected function GetHeading()
    {
        return '<strong>' . $this->jobModel->GetTitle() . '</strong>';
    }

    private function AddLine($title, $text)
    {
        echo '<dt>' . $title . '</dt>';
        echo '<dd>' . $text . '</dd>';
    }

    final protected function PrintBodyContent()
    {
        echo '<dl>';
        $jobModel = $this->jobModel;
        $this->AddLine('Location', $jobModel->GetLocation());
        echo '<br>';
        $this->AddLine('Company', $jobModel->GetCompany());
        echo '<br>';
        $this->AddLine('Description', $jobModel->GetDescription());
        echo '<br>';
        $this->AddLine('Requirements', $jobModel->GetRequirements());
        echo '<br>';
        $this->AddLine('Salary', $jobModel->GetSalary());
        echo '<br>';
        $this->AddLine('People Applied', $jobModel->GetPeopleApplied());
        echo '</dl>';
    }

    final protected function PrintFooterContent()
    {
        $user = UserService::GetInstance()->GetUser();

        echo '<div class="pull-left">';
        $statisticsButtonTitle = Tags::CreateSpan('', new HtmlAttributes(new Dictionary(array('class' => 'glyphicon glyphicon-stats'))));
        echo Tags::CreateLink($statisticsButtonTitle, '/company/' . $this->jobModel->GetCompany(), new HtmlAttributes(new Dictionary(array('class' => 'btn btn-default'))));
        echo '</div>';

        //right footer container - starts
        echo '<div class="pull-right">';

        //mini statistics
        echo '<div class="pull-left" style="margin-top: 5px;">';
        echo '<div class="label label-success" style="margin-right: 10px;"><span class="glyphicon glyphicon-arrow-up"></span><span> 10000 </span></div>';
        echo '<div class="label label-danger" style="margin-right: 10px;"><span class="glyphicon glyphicon-arrow-down"></span><span> 5000 </span></div>';
        echo '</div>';

        //buttons
        echo '<div class="btn-toolbar pull-right">';
        echo '<div class="btn-group">';
        $this->PrintLikeButtons();
        echo '</div>';
        echo '<div class="btn-group">';
        $this->PrintCommentButton();
        echo '</div>';
        echo '<div class="btn-group">';
        $this->PrintSaveButton();
        $this->PrintApplyButton();
        echo '</div>';
        echo '</div>';

        //right footer container - ends
        echo '</div>';

        echo '<div class="clearfix"></div>';
    }

    protected function PrintLikeButtons(HtmlAttributes $attributes = null)
    {
        echo '<a href="company/#id#/like" type="button" class="btn btn-default" .  . title="Like"><span class="glyphicon glyphicon-thumbs-up"></span></a>';
        echo '<a href="company/#id#/dislike" type="button" class="btn btn-default disabled" title="Dislike"><span class="glyphicon glyphicon-thumbs-down"></span></a>';
    }

    protected function PrintCommentButton(HtmlAttributes $attributes = null)
    {
        echo '<a href="company/#id#/comment" type="button" class="btn btn-default disabled" title="Comment"><span class=" glyphicon glyphicon-comment"></span></a>';
    }

    public function Update(Dictionary $savedJobs)
    {
        //TODO: override - make even abstract later on
    }

    protected function PrintSaveButton(HtmlAttributes $attributes = null)
    {
        $buttonAttributes = new HtmlAttributes();
        $buttonAttributes->Add('class', 'btn btn-default ajax_save');
        $buttonAttributes->Add('title', 'Save');
        $buttonAttributes->Add('id', $this->jobModel->GetGuid());

        if(null !== $attributes) {
            $buttonAttributes->AmendAttributes($attributes);
        }

        $spanAttributes = new HtmlAttributes();
        $spanAttributes->Add('class', 'glyphicon glyphicon-floppy-disk');
        $saveSpan = Tags::CreateSpan('', $spanAttributes);

        $saveButton = Tags::CreateButtonDefault($saveSpan, $buttonAttributes);
        echo $saveButton;
    }

    protected function PrintApplyButton(HtmlAttributes $attributes = null)
    {
        $buttonAttributes = new HtmlAttributes();
        $buttonAttributes->Add('class', 'btn btn-default ajax_apply');
        $buttonAttributes->Add('title', 'Apply');
        $buttonAttributes->Add('id', $this->jobModel->GetGuid());

        if(null !== $attributes) {
            $buttonAttributes->AmendAttributes($attributes);
        }

        $spanAttributes = new HtmlAttributes();
        $spanAttributes->Add('class', 'glyphicon glyphicon-send');
        $saveSpan = Tags::CreateSpan('', $spanAttributes);

        $saveButton = Tags::CreateButtonDefault($saveSpan, $buttonAttributes);
        echo $saveButton;
    }

}