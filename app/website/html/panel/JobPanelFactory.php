<?php

namespace website\html\panel;

use website\html\panel\job_panel\GuestJobPanel;
use website\html\panel\job_panel\JobSeekerJobPanel;
use website\models\account\JobModel;
use website\models\users\UserFactory;

final class JobPanelFactory
{

    /***
     * @param string $accountType
     * @param JobModel $jobModel
     * @return null|JobPanel
     * @throws \Exception
     */
    public static function Create($accountType, JobModel $jobModel)
    {
        $jobPanel = null;
        switch($accountType)
        {
            case UserFactory::ACCOUNT_TYPE_JOB_SEEKER:
                $jobPanel = new JobSeekerJobPanel($jobModel);
                break;
            case UserFactory::ACCOUNT_TYPE_GUEST:
                $jobPanel = new GuestJobPanel($jobModel);
                break;
            default:
                throw new \Exception('Job panel is not implemented for the specified user: ' . $accountType);
        }
        return $jobPanel;
    }

}