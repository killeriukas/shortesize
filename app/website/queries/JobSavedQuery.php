<?php

namespace website\queries;

use lib\data_structure\Dictionary;
use lib\database\Query;
use website\models\job\JobSavedModel;

final class JobSavedQuery extends Query
{
    public function CreateModel(Dictionary $data)
    {
        return new JobSavedModel($data);
    }
}