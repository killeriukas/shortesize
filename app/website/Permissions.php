<?php

namespace website;

use website\models\users\UserFactory;

final class Permissions
{
    public static function HasPermission($accountType, $currentPage)
    {
        $permissions = array(
            UserFactory::ACCOUNT_TYPE_GUEST => require 'permissions/GuestPermissions.php',
            UserFactory::ACCOUNT_TYPE_ADMIN => require 'permissions/AdminPermissions.php',
            UserFactory::ACCOUNT_TYPE_MODERATOR => array(),
            UserFactory::ACCOUNT_TYPE_BUSINESS_OWNER => require 'permissions/BusinessOwnerPermissions.php',
            UserFactory::ACCOUNT_TYPE_JOB_SEEKER => require 'permissions/JobSeekerPermissions.php'
        );

        $deniedPages = $permissions[$accountType];

        if (isset($deniedPages[$currentPage])) {
            return $deniedPages[$currentPage] === true;
        }
        return false;
    }

}

