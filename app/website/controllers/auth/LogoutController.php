<?php

namespace website\controllers\auth;

use lib\slim\AController;
use website\controllers\auth\user\UserService;

final class LogoutController extends AController
{

    public function IndexAction()
    {
        UserService::LogOut();

        //TODO: later on redirect to the main web page
        $this->app->redirectTo(WEB_LOGIN_GET);
    }

}