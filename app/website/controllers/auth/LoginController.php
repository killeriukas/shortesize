<?php

namespace website\controllers\auth;

use lib\slim\AController;
use website\controllers\auth\user\UserService;

final class LoginController extends AController
{

    public function IndexAction()
    {
        if (UserService::IsLoggedIn()) {
            $this->app->redirectTo(WEB_MY_ACCOUNT_GET);
        } else {
            $this->app->render('/' . PROJECT_AUTH_FOLDER . '/LoginView.php');
        }
    }

    public function LoginAction()
    {
        $data = $this->app->request->post();

        $isLogin = isset($data['login']);

        if ($isLogin) {
            $loginEmail = $data['email'];

            if (empty($loginEmail)) {
                $this->app->redirect('/auth/login');
            }

            $loginPassword = $data['password'];

            if (empty($loginPassword)) {
                $this->app->redirect('/auth/login');
            }

            $user = UserService::LoadUser(array('email' => $loginEmail));
            $userService = UserService::GetInstance();
            if (null !== $user) {
                $userService->SetUser($user);
                if ($userService->LogIn($loginPassword)) {
                    $this->app->redirectTo(WEB_MY_ACCOUNT_GET);
                } else {
                    echo 'Password is incorrect!';
                    return;
                }

            }
            echo 'User was not found! Redirect to login for again!';
        } else {
            $this->app->redirect('/auth/register');
        }
    }

}