<?php

namespace website\controllers\account\job\listings;

use lib\data_structure\Dictionary;
use lib\database\ModelLoader;
use lib\slim\AController;
use website\html\account\HtmlViewFactory;
use website\models\account\JobModel;
use website\models\account\JobModelQuery;

final class ListingsController extends AController
{

    public function IndexAction()
    {
//        $jobFactory = new ModelFactory(MongoDbSingleton::GetInstance());
//        $loadedCursor = $jobFactory->LoadData(JobModel::JOB_COLLECTION_ID, array('creator' => UserService::GetLoggedUserEmail()));
//        $jobModels = $jobFactory->CreateModels($loadedCursor, 'website\models\account\JobModel');

        $email = $this->user->GetEmail();
        $query = new JobModelQuery(JobModel::JOB_COLLECTION_ID, array('creator' => $email));
        $jobModels = ModelLoader::Load($query);

        $accountHtml = HtmlViewFactory::CreateHtmlViewByType($this->user->GetAccountType(), PROJECT_NAME . ' | Job Listings');

        $this->app->render('/' . PROJECT_ACCOUNT_FOLDER . '/job/listings/ListingsView.php', array('html' => $accountHtml, 'user' => $this->user, 'jobModels' => $jobModels));
    }

    public function CreateListingAction()
    {
        $accountHtml = HtmlViewFactory::CreateHtmlViewByType($this->user->GetAccountType(), PROJECT_NAME . ' | Create Job');
        $this->app->render('/' . PROJECT_ACCOUNT_FOLDER . '/job/listings/CreateView.php', array('html' => $accountHtml, 'user' => $this->user));
    }

    public function SaveListingAction()
    {
        $formData = new Dictionary($this->app->request->post());

        $job = new JobModel($formData);

        $email = $this->user->GetEmail();
        $job->SetCreator($email);
        $job->Save();

        $this->app->redirectTo(WEB_JOB_LISTINGS_GET);
    }

}