<?php

use website\controllers\auth\user\UserService;

$slimApp->get('/joblistings/:documentGuid', function ($documentGuid) use ($slimApp) {

    if (!UserService::IsLoggedIn()) {
        $slimApp->redirectTo(WEB_LOGIN_GET);
    }

    $document = new \website\models\account\JobModel();

    if ($document->LoadOne(array('creator' => UserService::GetLoggedUserEmail(), 'guid' => $documentGuid))) {
        $slimApp->render('/' . PROJECT_ACCOUNT_FOLDER . '/job/DocumentView.php', array('document' => $document));
    } else {
        $slimApp->render('/' . PROJECT_ACCOUNT_FOLDER . '/job/DocumentView.php');
    }
});