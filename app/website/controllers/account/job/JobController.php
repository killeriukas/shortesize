<?php

namespace website\controllers\account\job;

use lib\data_structure\Dictionary;
use lib\database\ModelLoader;
use lib\slim\AController;
use website\html\account\HtmlViewFactory;
use website\models\account\JobModel;
use website\models\job\JobSavedModel;
use website\models\users\UserModel;
use website\queries\JobDeleteQuery;
use website\queries\JobModelQuery;
use website\queries\JobSavedQuery;

final class JobController extends AController
{

    public function GetSavedJobsAction()
    {
        $query = new JobSavedQuery(JobSavedModel::JOB_SAVED_COLLECTION_ID, array('creatorId' => $this->user->GetUniqueId()));

        $jobModels = ModelLoader::Load($query);

        $accountHtml = HtmlViewFactory::CreateHtmlViewByType($this->user->GetAccountType(), PROJECT_NAME . ' | Saved Jobs');

        $this->app->render('/' . PROJECT_ACCOUNT_FOLDER . '/job/SavedView.php', array('html' => $accountHtml, 'user' => $this->user, 'jobModels' => $jobModels));
    }

    public function SaveJobAction()
    {
        $data = $this->app->request->post();

        $dataDict = new Dictionary($data);
        $dataDict->Add('creatorId', $this->user->GetUniqueId());

        $jobSavedModel = new JobSavedModel($dataDict);

        if($jobSavedModel->Exists()) {
            echo 'Cannot save the same model again!';
            return;
        }

        $query = new JobModelQuery(JobModel::JOB_COLLECTION_ID, array('guid' => $dataDict->Get(JobModel::UNIQUE_ID)));
        $jobModels = ModelLoader::Load($query);

        if(null === $jobModels) {
            echo 'Save failed, job listing is not found!';
            return;
        }

        $jobModel = $jobModels->Get(0);
        $jobSavedModel->Update($jobModel);

        $jobSavedModel->Save();
    }

    public function DeleteJobAction()
    {
        $data = $this->app->request->delete();
        $dataDict = new Dictionary($data);

        $jobSavedModel = new JobSavedModel($dataDict);

        if(!$jobSavedModel->Exists()) {
            echo 'Cannot delete non existing object!';
            return;
        }

        $deleteQuery = new JobDeleteQuery(JobSavedModel::JOB_SAVED_COLLECTION_ID, array(JobModel::UNIQUE_ID => $dataDict->Get(JobModel::UNIQUE_ID)));

        $hasDeleted = ModelLoader::Delete($deleteQuery);

        //hasDeleted can be bool or array. unfortunately, it is an array now :( needs fixing
    }

    public function GetAppliedJobsAction()
    {
        $accountHtml = HtmlViewFactory::CreateHtmlViewByType($this->user->GetAccountType(), PROJECT_NAME . ' | Saved Jobs');

        $this->app->render('/' . PROJECT_ACCOUNT_FOLDER . '/job/AppliedView.php', array('html' => $accountHtml, 'user' => $this->user, 'jobModels' => null));
    }

    public function ApplyJobAction()
    {
        $data = $this->app->request->post();

        $dataDict = new Dictionary($data);
        $dataDict->Add(UserModel::UNIQUE_ID, $this->user->GetUniqueId());

        //$jobAppliedModel = new JobSavedModel($dataDict);

        if($jobAppliedModel->Exists()) {
            echo 'Cannot apply to the same job again!';
            return;
        }



    }


}