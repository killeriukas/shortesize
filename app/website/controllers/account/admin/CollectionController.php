<?php

namespace website\controllers\account\admin;

use lib\data_structure\GeneralList;
use lib\database\MongoDbHelper\MongoDbSingleton;
use lib\slim\AController;
use website\controllers\auth\user\UserService;
use website\html\account\HtmlViewFactory;

final class CollectionController extends AController
{
    public function CollectionGetAction($collection_id)
    {
        $user = UserService::GetInstance()->GetUser();
        $accountHtml = HtmlViewFactory::CreateHtmlViewByType($user->GetAccountType(), PROJECT_NAME . ' | Collection');

        $collection = MongoDbSingleton::GetInstance()->GetCollection($collection_id);
        $mongoCursor = $collection->find();

        $userRecords = new GeneralList();
        while ($mongoCursor->hasNext()) {
            $mongoCursor->next();
            $currentRecord = $mongoCursor->current();
            $userRecords->Add($currentRecord);
        }

        $this->app->render('/' . PROJECT_ACCOUNT_FOLDER . '/admin/CollectionView.php',
            array('html' => $accountHtml,
                'user' => $user,
                'user_records' => $userRecords,
                'collection_id' => $collection_id
            )
        );
    }


}