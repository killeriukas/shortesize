<?php

namespace website\controllers\account\admin;

use lib\data_structure\Dictionary;
use lib\database\MongoDbHelper\MongoDbSingleton;
use lib\slim\AController;
use website\controllers\auth\user\UserService;
use website\html\account\HtmlViewFactory;

final class DatabaseController extends AController
{
    public function IndexAction()
    {
        $user = UserService::GetInstance()->GetUser();
        $accountHtml = HtmlViewFactory::CreateHtmlViewByType($user->GetAccountType(), PROJECT_NAME . ' | Database');

        $collectionNames = MongoDbSingleton::GetInstance()->GetCollections();

        $dbRecords = new Dictionary();

        foreach ($collectionNames as $name) {
            $collectionData = MongoDbSingleton::GetInstance()->GetCollection($name);
            $dbRecords->Add($name, $collectionData);
        }

        $this->app->render('/' . PROJECT_ACCOUNT_FOLDER . '/admin/DatabaseView.php',
            array('html' => $accountHtml,
                'user' => $user,
                'db_config' => $this->app->config('db'),
                'db_collections' => $collectionNames,
                'db_records' => $dbRecords
            )
        );
    }

}