<?php

namespace website\controllers\account;

use lib\slim\AController;
use website\controllers\auth\user\UserService;
use \website\html\account\HtmlViewFactory;

final class AccountController extends AController
{

    public function IndexAction()
    {
        $user = UserService::GetInstance()->GetUser();
        $accountHtml = HtmlViewFactory::CreateHtmlViewByType($user->GetAccountType(), PROJECT_NAME . ' | My Account');
        $this->app->render('/' . PROJECT_ACCOUNT_FOLDER . '/MyAccountView.php', array('html' => $accountHtml, 'user' => $user));
    }

}