<?php

namespace website\controllers\main;

use lib\database\ModelLoader;
use lib\slim\AController;
use website\html\account\HtmlViewFactory;
use website\models\account\JobModel;
use website\models\job\JobSavedModel;
use website\models\users\UserFactory;
use website\queries\JobModelQuery;
use website\queries\JobSavedQuery;

final class IndexController extends AController
{

    public function IndexAction()
    {
        $jobModelQuery = new JobModelQuery(JobModel::JOB_COLLECTION_ID);
        $jobModels = ModelLoader::Load($jobModelQuery);

        $accountHtml = HtmlViewFactory::CreateHtmlViewByType($this->user->GetAccountType(), PROJECT_NAME);

        $savedJobs = null;
        if($this->user->GetAccountType() === UserFactory::ACCOUNT_TYPE_JOB_SEEKER)
        {
            $saveModelQuery = new JobSavedQuery(JobSavedModel::JOB_SAVED_COLLECTION_ID);
            $savedJobs = ModelLoader::LoadArranged($saveModelQuery, JobModel::UNIQUE_ID);
        }

        $this->app->render('/' . PROJECT_MAIN_FOLDER . '/IndexView.php', array('html' => $accountHtml, 'jobModels' => $jobModels, 'user' => $this->user, 'savedJobs' => $savedJobs));
    }

}