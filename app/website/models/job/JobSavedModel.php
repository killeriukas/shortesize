<?php

namespace website\models\job;

use lib\data_structure\Dictionary;
use lib\database\MongoDbHelper\MongoDbModel;
use lib\database\MongoDbHelper\MongoDbSingleton;
use website\models\account\JobModel;

final class JobSavedModel extends MongoDbModel
{

    const JOB_SAVED_COLLECTION_ID = 'job_saved_model';

    public function __construct(Dictionary $parameters = null)
    {
        parent::__construct(MongodbSingleton::GetInstance(), JobSavedModel::JOB_SAVED_COLLECTION_ID, $parameters);
    }

    public function Update(JobModel $model)
    {
        $title = $model->GetTitle();
        $this->SetVariable('title', $title);

        $location = $model->GetLocation();
        $this->SetVariable('location', $location);

        $salary = $model->GetSalary();
        $this->SetVariable('salary', $salary);
    }

    public function GetSalary()
    {
        return $this->GetVariable('salary');
    }

    public function GetLocation()
    {
        return $this->GetVariable('location');
    }

    public function GetTitle()
    {
        return $this->GetVariable('title');
    }

    public function GetJobId()
    {
        return $this->GetVariable('job_id');
    }

    protected function SetSavedAt(\MongoDate $savedAt)
    {
        $this->SetVariable('saved_at', $savedAt);
    }

    /***
     * @return \MongoDate
     */
    public function GetSaveDate()
    {
        return $this->GetVariable('saved_at');
    }

    public function Save()
    {
        $this->SetSavedAt(new \MongoDate());
        parent::Save();
    }

    protected function GetUniquePair()
    {
        return array(JobModel::UNIQUE_ID => $this->GetJobId());
    }
}
