<?php

namespace website\models\users;

use lib\data_structure\Dictionary;

final class BusinessOwnerUserModel extends UserModel
{

    protected $companyName;

    public function __construct(Dictionary $parameters = null)
    {
        parent::__construct(UserFactory::ACCOUNT_TYPE_BUSINESS_OWNER, $parameters);
    }

    public function GetCompanyName()
    {
        return $this->companyName;
    }

    public function GetUserName()
    {
        return 'Business Man NAME';
    }
}