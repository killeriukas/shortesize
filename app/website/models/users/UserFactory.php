<?php

namespace website\models\users;

use lib\data_structure\Dictionary;

final class UserFactory
{
    const ACCOUNT_TYPE_JOB_SEEKER = 'jseeker';
    const ACCOUNT_TYPE_BUSINESS_OWNER = 'bowner';
    const ACCOUNT_TYPE_ADMIN = 'admin';
    const ACCOUNT_TYPE_MODERATOR = 'mod';
    const ACCOUNT_TYPE_GUEST = 'guest';

    public static function CreateGuestUser()
    {
        $information = new Dictionary();
        //TODO: add more info if required

        $user = self::CreateUserByType(self::ACCOUNT_TYPE_GUEST, $information);
        return $user;
    }

    public static function CreateUserByType($type, Dictionary $data = null)
    {
        $user = null;
        switch ($type) {
            case UserFactory::ACCOUNT_TYPE_JOB_SEEKER:
                $user = new JobSeekerUserModel($data);
                break;
            case UserFactory::ACCOUNT_TYPE_ADMIN:
                $user = new AdminUserModel($data);
                break;
            case UserFactory::ACCOUNT_TYPE_BUSINESS_OWNER:
                $user = new BusinessOwnerUserModel($data);
                break;
            case UserFactory::ACCOUNT_TYPE_MODERATOR:

                break;
            case UserFactory::ACCOUNT_TYPE_GUEST:
                $user = new GuestUserModel($data);
                break;
            default:
                throw new \Exception("User of type " . $type . " doesn't exist in the current context!");
        }

        return $user;
    }

}