<?php

namespace website\models\account;

use lib\data_structure\Dictionary;
use lib\database\MongoDbHelper\MongoDbModel;
use lib\database\MongoDbHelper\MongoDbSingleton;

class JobModel extends MongoDbModel
{
    const UNIQUE_ID = 'job_id'; //needs to change to job_id later on


    const JOB_MODEL_ID = 'job_model_id';
    const JOB_COLLECTION_ID = 'job_model';

    protected $title = '';
    protected $location = '';
    protected $company = '';

    protected $category = '';
    protected $description = '';
    protected $requirements = '';
    protected $extras = '';
    protected $salary = 0;


    protected $guid = '';

    protected $created = null;



    /**
     * @var \MongoDate
     */
    protected $lastUpdate = null;

    protected $peopleApplied = 0;
    protected $activated = false;
    protected $creator = '';

    public function __construct(Dictionary $parameters = null)
    {
        parent::__construct(MongodbSingleton::GetInstance(), JobModel::JOB_COLLECTION_ID, $parameters);

        if (is_null($this->created)) {
            $this->created = new \MongoDate();
        }

        if (empty($this->guid)) {
            $this->guid = \lib\shared\Functions::GenerateGuid();
        }

        if(empty($this->company))
        {
            $this->company = 'Unknown';
        }
    }

    public function GetGuid()
    {
        return $this->guid;
    }

    public function GetDescription()
    {
        return $this->description;
    }

    public function GetRequirements()
    {
        return $this->requirements;
    }

    public function GetCompany()
    {
        return $this->company;
    }

    public function IsActivated()
    {
        return $this->activated;
    }

    public function GetTitle()
    {
        return $this->title;
    }

    public function GetCreator()
    {
        return $this->creator;
    }

    public function GetSalary()
    {
        return $this->salary;
    }

    public function GetLastUpdated_Readable()
    {
        return $this->FormatDate($this->lastUpdate->sec);
    }

    private function FormatDate($secs)
    {
        return date('Y/m/d H:i:s', $secs);
    }

    public function GetCreated_Readable()
    {
        return $this->FormatDate($this->created->sec);
    }

    public function GetLocation()
    {
        return $this->location;
    }

    public function GetPeopleApplied()
    {
        return $this->peopleApplied;
    }

    public function SetCreator($creator)
    {
        $this->creator = $creator;
    }

    public function Save()
    {
        $this->lastUpdate = new \MongoDate();
        parent::Save();
    }

    protected function GetUniquePair()
    {
        throw new \BadMethodCallException('Not implemented exception!');
    }
}
