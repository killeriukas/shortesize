<?php

namespace website\middleware;

use Slim\Middleware;
use website\controllers\auth\user\UserService;
use website\models\users\UserFactory;

final class UserLoginMiddleware extends Middleware
{
    /**
     * Call
     *
     * Perform actions specific to this middleware and optionally
     * call the next downstream middleware.
     */
    public function call()
    {
        if (!UserService::IsLoggedIn()) {
            $user = UserFactory::CreateGuestUser();
            UserService::GetInstance()->SetUser($user);
        } else {
            $userId = UserService::GetLoggedInUserId();
            $user = UserService::LoadUser(array('uniqueId' => $userId));
            if (null === $user) {
                throw new \InvalidArgumentException('Used unique ID for user is invalid and user failed to load: ' . $userId);
            }
            UserService::GetInstance()->SetUser($user);
        }

        // Run inner middleware and application
        $this->next->call();
    }
}