<?php

namespace website\middleware;

use Slim\Middleware;
use Slim\Route;
use website\controllers\auth\user\UserService;
use website\Permissions;

final class UserPermissionMiddleware extends Middleware
{
    /**
     * Call
     *
     * Perform actions specific to this middleware and optionally
     * call the next downstream middleware.
     */
    public function call()
    {
        //TODO: would be much faster to directly access the URL based on the NAME -> but can't ;(
        $mathedRoutes = $this->app->router->getMatchedRoutes($this->app->request->getMethod(), $this->app->request->getResourceUri());

        //if wrong URL is passed, process the web page without checking the permission, since it will fail anyway
        if(count($mathedRoutes) == 0)
        {
            $this->next->call();
            return;
        }

        /**
         * @var Route $route
         */
        $route = $mathedRoutes[0];

        $user = UserService::GetInstance()->GetUser();

        if (!Permissions::HasPermission($user->GetAccountType(), $route->getName())) {
            $this->app->redirectTo(WEB_ERROR_ACCESS_DENIED);
        }

        // Run inner middleware and application
        $this->next->call();
    }
}