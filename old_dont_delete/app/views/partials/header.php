<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $common['title'] ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/MainPage.css">
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <h1>Shortesize</h1>
            </div>