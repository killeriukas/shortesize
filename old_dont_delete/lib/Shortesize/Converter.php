<?php
namespace Shortesize;

class Converter
{
    /**
     * @param $length - shortcut length
     * @return string - returns short random string
     */
    public static function getShortcut($length)
    {
        $alphabet = array_merge(range('a', 'z'), range('A', 'Z'), range('0', '9'));
        $alphabetCount = count($alphabet);

        $shortcut = '';
        for($i = 0; $i < $length; ++$i)
        {
            $shortcut .= $alphabet[rand(0, $alphabetCount - 1)];
        }

        return $shortcut;
    }


}
