<?php
return array(
    'slim' => array(
        'templates.path' => APP_BASE . '/app/views',
        'mode' => 'dev'
    ),
    'dev' => array(
        'db' => array(
            'name' => 'tmi_test',
            'collection' => 'collection_test'
        )
    ),
    'production' => array(
        'db' => array(
            'name' => 'tmi',
            'collection' => 'shortUrls'
        )
    )
);
