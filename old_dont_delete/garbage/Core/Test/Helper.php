<?php

class Helper
{

    public static function GetRandomShortcut($length)
    {
        $alphabet = array_merge(range('a', 'z'), range(0, 9)); //'qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890';
        $alphabetCount = count($alphabet);
        $shortcut = '';
        for ($i = 0; $i < $length; ++$i) {
            $shortcut .= $alphabet[rand(0, $alphabetCount - 1)];
        }
        return $shortcut;
    }

    public static function Redirect($newUrl, $http)
    {
        $prefix = '';
        if ($http === true) {
            if (strpos($newUrl, 'http') !== 0) {
                $prefix = 'http://';
            }
        } else {
            $prefix = '/';
        }

        header('Location: ' . $prefix . $newUrl);
    }


}