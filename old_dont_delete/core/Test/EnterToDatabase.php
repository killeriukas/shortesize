<?php

$title = $_POST['title'];
$desc = $_POST['description'];

if(isset($title) && strlen($title) != 0 && isset($desc) && strlen($desc) != 0)
{
    $db = new MongoClient();
    $coll = $db->selectCollection('tmi_test','posts');

    $query = array('title' => $title, 'description' => $desc);

    $foundDoc = $coll->findOne($query);

    if(isset($foundDoc))
    {
        echo 'Document already exists in the database';
        return;
    }

    $succ = $coll->insert($query);

   // var_dump($succ);

    if($succ['ok'] == true)
    {
        echo 'Record has been added to the database';
    }
}