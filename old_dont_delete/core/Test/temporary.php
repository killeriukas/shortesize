<!DOCTYPE html>
<html lang="en">
<head>
    <title>Shortesize URL</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../core/css/MainPage.css">
</head>
<body>
<header>
    <p>header</p>
</header>
<div class="container">
    <form role="form" action="../core/Test/ShortUrl.php" method="post">
        <div class="form-group">
            <label for="longWebsiteUrl">Long URL</label>
            <input type="text" class="form-control" id="longWebsiteUrl" placeholder="Copy URL here" name="longUrl">
        </div>
        <div class="align-center margin-bottom">
            <button type="submit" class="btn btn-default" name="makeShort">Short</button>
            <button type="submit" class="btn btn-default" name="makeSafe">
                <del>Safe</del>
            </button>
        </div>
    </form>
    <?php
    if (isset($_SESSION['success'])) {
        if ($_SESSION['success'] === false) {
            echo '<div class="alert alert-danger" role="alert"><strong>Message: </strong>' . $_SESSION['message'] . '</div>';
        } else if ($_SESSION['success'] === true) {
            echo '
                <div class="panel panel-success">
                    <div class="panel-heading"><strong>Short URL</strong></div>
                    <div class="panel-body">
                    <kbd>Press Ctrl+C to copy:</kbd>
                    <input type="text" value="' . $_SESSION['message'] . '" id="succMsg" size="'. strlen($_SESSION['message']) . '" style="text-align: center; " readonly>
                    </div>
                </div>';
        }
        unset($_SESSION['success']);
        unset($_SESSION['message']);
    }
    ?>
</div>
<footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {

            $("input#succMsg").click(function () {
                //  alert('boobs');
                //  $(this).focus();
                $(this).select();
            });

            // alert( $("input#succMsg").text());

//            $("span#succMsg").focus();
//
//            $("span#succMsg").select();
        });
    </script>
    <!--    <script type="text/javascript" src="scripts/jquery.zclip.js"></script>-->
    <!--    <script>-->
    <!--        $(document).ready(function(){-->
    <!--            $("button#copy-btn").zclip({-->
    <!--                path: "/scripts/ZeroClipboard.swf",-->
    <!--                copy: $("button#copy-btn").text($("div#succMsg").text());-->
    <!--            });-->
    <!---->
    <!--            //  $("button#copy-btn").zclip({-->
    <!--//                path:"scripts/ZeroClipboard.swf",-->
    <!--//                copy:function(){return $("button#copy-btn").text('test');-->
    <!--//                });-->
    <!--        });-->
    <!--    </script>-->
    <p>footer</p>
</footer>
</body>
</html>