<?php

return array(

	// Index method
	'welcome_text' => 'Welkom',
	'welcome_message' => '
		Hallo, welkom van de welcome controllers! <br/>
		Deze content kan in worden veranderd <code>/app/views/welcome/welcome.php</code>
	',

	// Subpage method
	'subpage_text' => 'Subpagina',
	'subpage_message' => '
		Hallo, welkom van de welcome controllers en methoden subpagina! <br/>
		Deze content kan in worden veranderd <code>/app/views/welcome/subpage.php</code>
	',

	// Buttons
	'open_subpage' => 'Open subpagina',
	'back_home' => 'Home',

);