# Title
Job Board

# Description
Yet another jobs board, but this time more focused around the employee.

# Overview
Find job listings. See companies and their statistics. Check their response rate. Check if there is a response at all.
Check the comments of previous and current employees. Ask questions. Make up your mind by not just reading people's biased comments, but also seeing facts.
Everything is in one place. Never been easier. See your application progress from start to finish. Track your future.

# Account types
* Job seeker
* Business Owner
* Administrator
* Manager