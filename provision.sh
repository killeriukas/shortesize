#!/usr/bin/env bash
sudo locale-gen en_GB.UTF-8
echo "Setting locale to en_GB.UTF-8";
apt-get update
echo "Preparing to install Apache server";
apt-get install -y apache2
echo "Apache server has been installed. Configuring...";
rm -rf /var/www
ln -sf /vagrant/public /var/www
sudo a2dissite 000-default.conf
sudo cp /vagrant/apache.conf /etc/apache2/sites-available/shortesize.conf
sudo a2ensite shortesize.conf
echo "Apache server has been configured. Preparing to install PHP5";
apt-get install -y php5
echo "PHP5 has been installed. Installing MongoDB";
apt-get install -y mongodb php5-mongo
echo "MongoDB has been successfully installed. PHP5 drivers for MongoDB have been installed too";
sed -e "\$a; Mongo Extension\nextension=mongo.so" /etc/php5/apache2/php.ini
echo "Adding mongo.so to php.ini"

echo "Installing mcrypt"
apt-get install -y php5-mcrypt

# Enable mcrypt library
sudo php5enmod mcrypt

# Enable rewrite module
sudo a2enmod rewrite
echo "Enable Mod Rewrite"
# Restart Apache
sudo service apache2 restart
echo "Restarting Apache"